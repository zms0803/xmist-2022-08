package com.example.demo.dao;

import com.example.demo.entity.TestDO;

import java.util.List;

public interface TestDao {
    List<TestDO>queryAll();
}
