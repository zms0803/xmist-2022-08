package com.example.demo.service.impl;

import com.example.demo.dao.TestDao;
import com.example.demo.entity.TestDO;
import com.example.demo.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestDao testDao;

    @Override
    public List<TestDO>queryAll(){
        return testDao.queryAll();
    }
}
