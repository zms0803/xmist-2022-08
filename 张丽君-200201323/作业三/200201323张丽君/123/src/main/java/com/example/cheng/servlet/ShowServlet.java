package com.example.cheng.servlet;

import com.example.cheng.dao.StuDao;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet(name = "ShowServlet", value = "/ShowServlet")
public class ShowServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String role=request.getParameter("role");
        String stuno=request.getParameter("stuno");
        List list=null;
        try {
            if (role.equals("1")){
                list= StuDao.getAllStu(null);//获取表中所有用户信息
            }else if (role.equals("0")){
                list= StuDao.getAllStu(stuno);//获取表中所有用户信息
            }
            //创建session对象:request.getSession()
            request.getSession().setAttribute("list",list);
            response.sendRedirect("thirteen/main.jsp?role="+role);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
