package com.example.cheng.dao;

import com.example.cheng.entity.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BaseDao {

    private static final String URL="jdbc:mysql://localhost:3306/mystu?useUnicode=true&characterEncoding=utf-8";//连接的url
    private static final String USER="root";//用户名
    private static final String PASS="123";//登录mysql的密码

    //获得连接对象的方法
    public static Connection getConn(){
        Connection connection = null;
        try {
            //1，注册驱动
            //去"com.mysql.jdbc"这个路径下找Driver类，将其加载到内存中
            Class.forName("com.mysql.jdbc.Driver");
            //2，获得连接
            //获得试图建立到指定数据库 URL 的连接
            connection= DriverManager.getConnection(URL,USER,PASS);
            System.out.println("连接成功！");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;//返回给调用者连接对象
    }
}
//static关键字的用途
//     一句话描述就是：方便在没有创建对象的情况下进行调用(方法/变量)。