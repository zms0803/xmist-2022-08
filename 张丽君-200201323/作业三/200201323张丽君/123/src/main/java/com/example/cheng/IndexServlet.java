package com.example.cheng;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

//注解
@WebServlet(name = "IndexServlet", value = "/IndexServlet")
public class IndexServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name=request.getParameter("username");
//        response.sendRedirect("main,jsp");
        //1，jsp内置对象out,在servlet怎么创建
//        PrintWriter out=response.getWriter();
        //response.getWriter()打印输出流
//        out.println("name:"+name);//网页上输出
        //2，jsp内置对象session,在servlet怎么创建
//        HttpSession servlet=request.getSession();
        //request.getSession()可以帮你得到HttpSession类型的对象
//        servlet.setAttribute("name",name);
//        response.sendRedirect("main.jap");
        //3.application
        //application用于数据共享
        ServletContext servletContext= this.getServletContext();
        servletContext.setAttribute("name",name);
        response.sendRedirect("seven/main.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
    //销毁
    @Override
    public void destroy() {
        super.destroy();
    }
}
