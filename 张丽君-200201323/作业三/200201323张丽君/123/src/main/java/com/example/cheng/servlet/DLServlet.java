package com.example.cheng.servlet;

import com.example.cheng.dao.StuDao;
import com.example.cheng.entity.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DLServlet", value = "/DLServlet")
public class DLServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String stuno =request.getParameter("stuno");
        String stuname=request.getParameter("stuname");
        String role=request.getParameter("role");
        Student student=new Student(stuno,stuname,Integer.parseInt(role));
        try {
            boolean flag= StuDao.checkStu(student);
            if (flag==true){
                request.getSession().setAttribute("role",role);//存储角色信息
                response.sendRedirect("ShowServlet?role="+role+"&stuno="+stuno);
            }else {
                response.getWriter().println("<script>alert('输入有误！请重新输入！');window.location='index.jsp'</script>");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
