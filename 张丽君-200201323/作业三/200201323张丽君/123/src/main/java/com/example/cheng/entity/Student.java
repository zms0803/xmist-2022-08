package com.example.cheng.entity;

public class Student {
    private String no;
    private String name;
    private String tel;
    private int role;

    public Student(String no, String name, String tel, int role) {
        this.no = no;
        this.name = name;
        this.tel = tel;
        this.role = role;
    }

    public Student(String no, String name, int role) {
        this.no = no;
        this.name = name;
        this.role = role;
    }

    public Student(String no, String name, String tel) {
        this.no = no;
        this.name = name;
        this.tel = tel;
    }

    public String getNo() {
        return no;
    }

    public String getName() {
        return name;
    }

    public String getTel() {
        return tel;
    }

    public int getRole() {
        return role;
    }
}
