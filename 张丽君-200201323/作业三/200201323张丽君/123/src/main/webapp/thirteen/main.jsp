<%@ page import="java.util.List" %>
<%@ page import="com.example.cheng.entity.Student" %><%--
  Created by IntelliJ IDEA.
  User: cheng
  Date: 2022/3/22
  Time: 17:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table width="409" border="1">
    <tr>
        <th width="69">账号</th>
        <th width="62">密码</th>
        <%
            String role= (String) session.getAttribute("role");//获取角色信息
            if (role.equals("1")){
        %>
        <th width="120">是否为管理员</th>
        <%
            }
        %>
    </tr>
    <%
        List list= (List) session.getAttribute("list");
        for (int i=0;i<list.size();i++) {
            Student student= (Student) list.get(i);//通过下标i获取集合类中的对象
    %>
    <tr>
        <th><%=student.getNo()%></th>
        <th><%=student.getName()%></th>
        <%
            if (role.equals("1")){
        %>
        <th><%=student.getRole()%></th>
        <%
            }
        %>
    </tr>
    <%
        }
    %>
</table>
<br/>
<a href="../index.jsp">退出</a>
<br/>
</body>
</html>
