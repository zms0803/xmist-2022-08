<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.example.cheng.User" %><%--导入--%>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<%--<% %>里面放置java代码  例下：--%>
<%
    String name = "张三";
    int age = 18;
    int score = 88;
    for (int i = 0; i <= 10; i++) {
        //System.out.println(i);//控制台输出
        //jsp内置对象9个，out输出
//        out.println(i+"<br>");
        out.println(i);
%>
<br><%--换行--%>
<%
    }
%>

<p>
    下面是个表格，行由循环控制
</p>
<table border="1" width="150">
    <%
        for (int i = 0; i < 8; i++) {
    %>
        <tr>
            <td><%=i+1%></td><%--等同于——> <td><% out.print(i+1); %></td>--%>
            <td>hello<%=name%></td>
        </tr>
    <%
        }
    %>
    <p>
        <%
            User user=new User();//alt+enter引用，快速导入类
            user.setName("admin");
            out.print(user.getName());
        %>
    </p>
</table>

<%--<h1><%= "Hello World!" %>--%>
<%--</h1>--%>
<%--<br/>--%>
<%--<a href="hello-servlet">Hello Servlet</a>--%>
</body>
</html>