<%--
  Created by IntelliJ IDEA.
  User: cheng
  Date: 2022/3/27
  Time: 14:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<h1>登录界面</h1>
<form action="<%=request.getContextPath()%>/DLServlet" method="post">
    <table width="234" height="115" border="1">
        <tr>
            <td width="53">学号：</td>
            <td width="131"><label>
                <input type="text" name="stuno" />
            </label></td>
        </tr>
        <tr>
            <td>姓名：</td>
            <td><label>
                <input type="text" name="stuname" />
            </label></td>
        </tr>
        <tr>
            <td>角色：</td>
            <td><label>
                <select name="role">
                    <option value="1">管理员</option>
                    <option value="0">普通用户</option>
                </select>
            </label></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><label>
                <input type="submit" name="Submit" value="登录" />
            </label></td>
        </tr>
    </table>
    <br/>
    <a href="eleven/index.jsp">进入注册界面</a>
</form>
</body>
</html>
