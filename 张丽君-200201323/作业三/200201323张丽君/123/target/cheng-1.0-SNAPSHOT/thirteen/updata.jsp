<%--
  Created by IntelliJ IDEA.
  User: cheng
  Date: 2022/3/24
  Time: 22:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    String stuno=request.getParameter("stuno");
    String stuname=request.getParameter("stuname");
    String tel=request.getParameter("tel");
%>
<h1>修改界面</h1>
<form action="<%=request.getContextPath()%>/UpdataServlet" method="post">
    <table width="327" height="130" border="1">
        <tr>
            <td width="45" height="33">学号:</td>
            <td width="139"><label>
                <input type="text" name="stuno" value="<%=stuno%>" />
            </label></td>
        </tr>
        <tr>
            <td>姓名:</td>
            <td><label>
                <input type="text" name="stuname" value="<%=stuname%>" />
            </label></td>
        </tr>
        <tr>
            <td>电话:</td>
            <td><label>
                <input type="text" name="tel" value="<%=tel%>"/>
            </label></td>
        </tr>
        <%
            String role= (String) session.getAttribute("role");//获取角色信息
            if (role.equals("1")){
        %>
        <tr>
            <td>角色：</td>
            <td><label>
                <select name="role">
                    <option value="1">管理员</option>
                    <option value="0">普通用户</option>
                </select>
            </label></td>
        </tr>
        <%
            }
        %>
        <tr>
            <td>&nbsp;</td>
            <td><label>
                <input type="submit" name="Submit" value="修改" />
            </label></td>
        </tr>
    </table>
</form>
</body>
</html>
