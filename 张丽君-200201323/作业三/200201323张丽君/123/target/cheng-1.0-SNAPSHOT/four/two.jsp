<%--
  Created by IntelliJ IDEA.
  User: 27254
  Date: 2022/3/3
  Time: 8:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    request.setCharacterEncoding("utf-8");
//    获取跳转前页面的request对象
    String str=request.getParameter("id");
    if (str.equals("hello"))
    {
//        跳转页面，请求转发方式（跳转页面时带着原来页面，不会重建）
        request.getRequestDispatcher("success.jsp").forward(request,response);
    }else {
//        跳转页面，重定向
        response.sendRedirect("fail.jsp");
    }
%>
</body>
</html>
