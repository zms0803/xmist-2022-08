<%--
  Created by IntelliJ IDEA.
  User: 27254
  Date: 2022/3/2
  Time: 16:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--<%
    out.print(request.getRemoteAddr()+"<br>");
    out.print(request.getRequestURL()+"<br>");
    out.print(request.getServerPort()+"<br>");
%>--%>

<h1>获取客户端发送给服务器的参数值</h1>
<%
    request.setCharacterEncoding("utf-8");//    处理中文乱码
//    getParameter(String name)获取客户端发送给服务器的参数值
    String username=request.getParameter("username");
    String pass=request.getParameter("pass");
    out.print(username+"<br>");
    out.print(pass+"<br>");
//    String[] getParameterValues(String name)获得如checkbox类(名字相同，但值有多个)的数据
    String[] ck=request.getParameterValues("checkbox");
    for (String s:ck){
        out.print(s+"<br>");
    }
%>
</body>
</html>
