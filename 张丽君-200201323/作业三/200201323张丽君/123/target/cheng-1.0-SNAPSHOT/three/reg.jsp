<%--
  Created by IntelliJ IDEA.
  User: 27254
  Date: 2022/3/2
  Time: 19:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="test.jsp" method="post"><%--  get输入会在地址上显示post不会在地址上显示--%>
<%--    action动作，当前页面提交的动作，提交到哪一个页面--%>
    用户名：
    <label>
<%--    提交给服务器的参数名username，参数值即为用户输入的内容,也就是value对应的值--%>
        <input type="text" name="username" value="admin" />
    </label>
    <br>
    密  码：
    <label>
        <%--    提交给服务器的参数名pass，参数值即为用户输入的内容,也就是value对应的值--%>
        <input type="password" name="pass" value="" />
    </label>
    <br>
    <p>爱 好：
        <input name="checkbox" type="checkbox" value="aaa" />
        aaa
        <input name="checkbox" type="checkbox" value="bbb" />
        bbb
        <input name="checkbox" type="checkbox" value="ccc" />
        ccc
    </p>
    <p>
        <label>
            <input type="submit" name="Submit" value="提交" />
        </label>
    </p>
</form>
</body>
</html>
