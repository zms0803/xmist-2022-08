<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.example.cheng.User" %><%--导入--%>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<%--建目录后跳转Servlet前面加上  <%=request.getContextPath()%>/  --%>
<%--webapp目录下则不加--%>
<h1>注册页面</h1>
<form id="form1" name="form1" method="post" action="<%=request.getContextPath()%>/RegServlet">
    <table width="313" border="1">
        <tr>
            <td width="66">学号：</td>
            <td width="231"><label>
                <input type="text" name="stuno" />
            </label></td>
        </tr>
        <tr>
            <td>姓名：</td>
            <td><label>
                <input type="text" name="stuname" />
            </label></td>
        </tr>
        <tr>
            <td>电话：</td>
            <td><label>
                <input type="text" name="tel" />
            </label></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td><label>
                <input type="submit" name="Submit" value="注册" />
            </label></td>
        </tr>
    </table>
    <a href="../index.jsp">退出</a>
</form>
</body>
</html>