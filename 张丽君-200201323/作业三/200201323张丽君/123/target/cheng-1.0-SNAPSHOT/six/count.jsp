<%--
  Created by IntelliJ IDEA.
  User: 27254
  Date: 2022/3/4
  Time: 11:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>当前页面访问量</h1>
<%
    if (application.getAttribute("count")!=null){
        //再次(先取值，再++)
        int count= (int) application.getAttribute("count");
        count++;
        application.setAttribute("count",count);
        out.println("访问量："+count);
    }else {
        //第一次
        application.setAttribute("count",1);
        out.println("访问量："+1);
    }
%>
</body>
</html>
