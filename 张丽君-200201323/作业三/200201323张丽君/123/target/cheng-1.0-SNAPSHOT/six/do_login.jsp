<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collections" %><%--
  Created by IntelliJ IDEA.
  User: 27254
  Date: 2022/3/4
  Time: 10:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    String name=request.getParameter("username");
    //"全局变量"application存储用户名
    if(application.getAttribute("nameList")!=null){
        //再次添加,先取出application中的集合类
        List nameList2= (List) application.getAttribute("nameList");
        //添加新的登录名
        nameList2.add(name);
        //再将集合类添加到application中
        application.setAttribute("nameList",nameList2);
    }else {
        //第一次添加,创建个新集合类，存储第一个名字；
        List nameList=new ArrayList();//新的集合类
        nameList.add(name);
        //再将集合类添加application中
        application.setAttribute("nameList",nameList);
    }
    response.sendRedirect("main.jsp");
%>
</body>
</html>
