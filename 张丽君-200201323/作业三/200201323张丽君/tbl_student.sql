/*
Navicat MySQL Data Transfer

Source Server         : cheng
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : mystu

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2022-03-30 11:01:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_student`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_student`;
CREATE TABLE `tbl_student` (
  `stuno` varchar(255) NOT NULL,
  `stuname` varchar(255) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL,
  `role` int(2) DEFAULT '0',
  PRIMARY KEY (`stuno`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_student
-- ----------------------------
INSERT INTO `tbl_student` VALUES ('1', '123', null, '0');
INSERT INTO `tbl_student` VALUES ('2', '123', null, '0');
INSERT INTO `tbl_student` VALUES ('3', '456', null, '0');
INSERT INTO `tbl_student` VALUES ('4', '123', null, '0');
INSERT INTO `tbl_student` VALUES ('root', 'root', null, '1');
