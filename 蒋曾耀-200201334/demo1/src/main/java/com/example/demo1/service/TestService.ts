package com.example.demo2.service;
import com.example.demo2.entity.TestDO;
import java.util.List;
/**
 * @author zhangmsh 2022/3/19
 * @since 1.0.0
 */
public interface TestService {
    List<TestDO> queryAll();
}
