package com.example.demo2.dao;
import com.example.demo2.entity.TestDO;
import java.util.List;
/**
 * @author zhangmsh 2022/3/19
 * @since 1.0.0
 */
public interface TestDao {
    List<TestDO> queryAll();
    @SpringBootApplication
    @MapperScan("com.example.demo2.dao")
    public class Demo2Application {
        public static void main(String[] args)
        {
            SpringApplication.run(Demo2Application.cla
                    ss, args);
        }
    }


