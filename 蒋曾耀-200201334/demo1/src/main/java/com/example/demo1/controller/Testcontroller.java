package com.example.demo2.controller;
import com.example.demo2.entity.TestDO;
import
        com.example.demo2.service.TestService;
import java.util.List;
import
        org.springframework.beans.factory.annotatio
n.Autowired;
import
        org.springframework.web.bind.annotation.Req
uestMapping;
import
        org.springframework.web.bind.annotation.Res
tController;
@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    private TestService testService;
    @RequestMapping("/test")
    public List<TestDO> test() {
        return testService.queryAll();
    }
}

