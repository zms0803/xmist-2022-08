package edu.xmist.demo1.service.impl;

import edu.xmist.demo1.dao.TestDao;
import edu.xmist.demo1.entity.TestDO;
import edu.xmist.demo1.service.TestService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zhangmsh 2022/3/19
 * @since 1.0.0
 */
@Service
public class TestServiceImpl implements TestService {

    @Autowired
    private TestDao testDao;

    @Override
    public List<TestDO> queryAll() {
        return testDao.queryAll();
    }
}
