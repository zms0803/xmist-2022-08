package edu.xmist.demo1.service;

import edu.xmist.demo1.util.Result;

/**
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
public interface LoginService {

    Result<String> managerLogin(String account, String pwd);
}
