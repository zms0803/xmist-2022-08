package edu.xmist.demo1.service.impl;

import edu.xmist.demo1.dao.ManagerDao;
import edu.xmist.demo1.entity.TManagerDO;
import edu.xmist.demo1.service.LoginService;
import edu.xmist.demo1.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private ManagerDao managerDao;

    @Override
    public Result<String> managerLogin(String account, String pwd) {
        TManagerDO manager = managerDao.getByAccount(account);
        if(manager == null){
            return Result.fail("管理员不存在");
        }
        if(!manager.getPwd().equals(pwd)){
            return Result.fail("账号或密码有误");
        }
        return Result.success(manager.getAccount());
    }
}
