package edu.xmist.demo1.entity;

/**
 * 管理员表
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
public class TManagerDO {

    private int id;
    /**
     * 账号
     */
    private String account;
    /**
     * 密码
     */
    private String pwd;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAccount() {
        return this.account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPwd() {
        return this.pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
}
