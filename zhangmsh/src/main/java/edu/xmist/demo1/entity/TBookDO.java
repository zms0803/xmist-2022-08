package edu.xmist.demo1.entity;

/**
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
public class TBookDO {

    private int id;
    /**
     * 名称
     */
    private String bookName;
    /**
     * 简介
     */
    private String intro;
    /**
     * 封面
     */
    private String icon;
    /**
     * 出版社
     */
    private String press;
    /**
     * 库存
     */
    private int stock;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBookName() {
        return this.bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getIntro() {
        return this.intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getPress() {
        return this.press;
    }

    public void setPress(String press) {
        this.press = press;
    }

    public int getStock() {
        return this.stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
