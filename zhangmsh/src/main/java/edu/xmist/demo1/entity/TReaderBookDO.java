package edu.xmist.demo1.entity;

/**
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
public class TReaderBookDO {

    private int id;
    private int readerId;
    private int bookId;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReaderId() {
        return this.readerId;
    }

    public void setReaderId(int readerId) {
        this.readerId = readerId;
    }

    public int getBookId() {
        return this.bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }
}
