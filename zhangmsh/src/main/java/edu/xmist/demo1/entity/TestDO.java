package edu.xmist.demo1.entity;

/**
 * 对应的表
 * @author zhangmsh 2022/3/19
 * @since 1.0.0
 */
public class TestDO {

    private int id;
    private String name;

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
