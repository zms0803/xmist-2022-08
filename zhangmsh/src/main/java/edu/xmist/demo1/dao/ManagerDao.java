package edu.xmist.demo1.dao;

import edu.xmist.demo1.entity.TManagerDO;
import org.apache.ibatis.annotations.Param;

/**
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
public interface ManagerDao {

    TManagerDO getByAccount(@Param("account") String account);
}
