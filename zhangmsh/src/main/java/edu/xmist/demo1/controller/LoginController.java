package edu.xmist.demo1.controller;

import edu.xmist.demo1.service.LoginService;
import edu.xmist.demo1.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping("/manager")
    public Result<String> managerLogin(String account, String pwd) {
        if (account == null || account.isEmpty()) {
            return Result.fail("账号不能为空");
        }
        if (pwd == null || pwd.isEmpty()) {
            return Result.fail("密码不能为空");
        }
        return loginService.managerLogin(account, pwd);
    }
}
