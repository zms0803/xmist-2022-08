package edu.xmist.demo1.controller;

import edu.xmist.demo1.entity.TestDO;
import edu.xmist.demo1.service.TestService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangmsh 2022/3/19
 * @since 1.0.0
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping("/test")
    public List<TestDO> test() {
        return testService.queryAll();
    }
}
