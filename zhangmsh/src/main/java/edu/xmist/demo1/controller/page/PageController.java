package edu.xmist.demo1.controller.page;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
@Controller
@RequestMapping("/page")
public class PageController {

    @RequestMapping("/login")
    public String login() {
        return "login";
    }
}
