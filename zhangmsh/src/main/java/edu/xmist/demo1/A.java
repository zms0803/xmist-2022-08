package edu.xmist.demo1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author zhangmsh 2022/3/19
 * @since 1.0.0
 */
public class A {

    public static void main(String[] args) {
        int min = 200201322;
        int max = 200201368;//63

        List<Integer> list = new ArrayList<>();
        for (int i = min; i <= max; i++) {
            list.add(i);
        }
        list.add(2030201017);
        list.add(2030201016);
        list.add(2030201015);
        list.add(2030201014);
        list.add(2030201013);

        Collections.shuffle(list);

        for (int i = 0; i < 8; i++) {
            System.out.println(list.get(i));
        }
    }
}