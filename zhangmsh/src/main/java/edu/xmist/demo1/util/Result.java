package edu.xmist.demo1.util;

/**
 * @author zhangmsh 2022/3/26
 * @since 1.0.0
 */
public class Result<T> {

    private boolean success;
    private T data;
    private String message;

    public static <T> Result<T> success(T data) {
        Result result = new Result();
        result.setSuccess(true);
        result.setData(data);
        result.setMessage("操作成功");
        return result;
    }

    public static Result fail(String message) {
        Result result = new Result();
        result.setSuccess(false);
        result.setData(null);
        result.setMessage(message);
        return result;
    }

    public static Result fail() {
        return fail("操作失败");
    }

    public boolean isSuccess() {
        return this.success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
