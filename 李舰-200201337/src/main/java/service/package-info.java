package service;
package com.example.demo2.service.impl;
import com.example.demo2.dao.TestDao;
import com.example.demo2.entity.TestDO;
import
        com.example.demo2.service.TestService;
import java.util.List;
import org.springframework.beans.factory.annotatio n.Autowired;
import org.springframework.stereotype.Service;
@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestDao testDao;

    @Override
    public List<TestDO> queryAll() {
        return testDao.queryAll();
    }
}