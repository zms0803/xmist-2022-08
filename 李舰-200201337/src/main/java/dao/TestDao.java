package dao;

import com.example.demo2.entity.TestDO;
import java.util.List;

public interface TestDao {
    List<TestDO> queryAll();
}
