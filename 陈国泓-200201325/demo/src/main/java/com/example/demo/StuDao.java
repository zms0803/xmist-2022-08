package com.example.demo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StuDao {
    public static boolean checkStu(Student student) throws SQLException {
        Connection connection=BaseDao.getConn();
        String sql="select * from tbl_student where stuno=? and stuname=?";
        PreparedStatement pst=connection.prepareStatement(sql);
        pst.setString(1,student.getName());
        pst.setString(2,student.getPass());
        ResultSet resultSet=pst.executeQuery();
        if (resultSet.next()){
            return true;
        }
        pst.close();
        resultSet.close();
        connection.close();
        return false;
    }

    public static List getAllStu() throws SQLException {
        List list=new ArrayList();
        Connection connection=BaseDao.getConn();
        String sql="select * from tbl_student";
        PreparedStatement pst=connection.prepareStatement(sql);
        ResultSet resultSet=pst.executeQuery();
        while (resultSet.next()){
            String name=resultSet.getString("name");
            String pass=resultSet.getString("pass");
            Student student=new Student(name,pass);
            list.add(student);
        }
        pst.close();
        resultSet.close();
        connection.close();
        return list;
    }

}
