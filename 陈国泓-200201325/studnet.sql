/*
Navicat MySQL Data Transfer

Source Server         : cheng
Source Server Version : 50528
Source Host           : localhost:3306
Source Database       : mystu

Target Server Type    : MYSQL
Target Server Version : 50528
File Encoding         : 65001

Date: 2022-04-01 17:50:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `studnet`
-- ----------------------------
DROP TABLE IF EXISTS `studnet`;
CREATE TABLE `studnet` (
  `name` varchar(255) NOT NULL,
  `pass` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of studnet
-- ----------------------------
