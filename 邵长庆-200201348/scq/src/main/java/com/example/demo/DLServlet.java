package com.example.demo;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "DLServlet", value = "/DLServlet")
public class DLServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name =request.getParameter("name");
        String pass=request.getParameter("pass");
        Student student=new Student(name,pass);
        try {
            boolean flag= StuDao.checkStu(student);
            if (flag==true){
                response.sendRedirect("ShowServlet");
            }else {
                response.getWriter().println("<script>alert('输入有误！请重新输入！')</script>");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
