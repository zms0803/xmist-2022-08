package cn.edu.xmist.demo.controller;

import cn.edu.xmist.demo.service.HelloService;
import cn.edu.xmist.demo.service.impl.HelloServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/hello")
public class HelloController {
    @Autowired
    private HelloService helloService;
    @RequestMapping("/say")
    public String say(String name) {
        if (name == null || name.isEmpty()) { return "您输入有误"; }
        return helloService.say(name);
    }
}
