package cn.edu.xmist.demo.service.impl;

import cn.edu.xmist.demo.dao.HelloDao;
import cn.edu.xmist.demo.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloService {
    @Autowired
    private HelloDao helloDao;
    @Override
    public String say(String name) {
        if ("body".equals(name)){ return "这是一个很阳光的男孩";
        }else if("girl".equals(name)){ return "哇...";
        }
        return helloDao.say(name);

    }
}
