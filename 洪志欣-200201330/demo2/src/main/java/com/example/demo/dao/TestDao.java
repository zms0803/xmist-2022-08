package com.example.demo.dao;
import com.example.demo.entity.TestDo;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface TestDao { List<TestDo> queryAll(); }
