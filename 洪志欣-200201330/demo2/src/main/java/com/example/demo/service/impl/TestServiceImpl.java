package com.example.demo.service.impl;
import com.example.demo.dao.TestDao;
import com.example.demo.entity.TestDo;
import com.example.demo.service.TestService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class TestServiceImpl implements TestService{
    @Autowired
    private TestDao testDao;
    @Override
    public List<TestDo> queryAll() {
        return testDao.queryAll();
    }
}

