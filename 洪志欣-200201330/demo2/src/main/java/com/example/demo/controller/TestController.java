package com.example.demo.controller;
import com.example.demo.entity.TestDo;
import com.example.demo.service.TestService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    private TestService testService;
    @RequestMapping("/test")
    public List<TestDo> test() { return testService.queryAll(); }

}
